import numpy as np
import matplotlib.pyplot as plt

E_0 = 0
V   = 0.3
E_exp = 1.058

Emin = -2
Emax = 2
n    = 10000
E_prime = np.linspace(Emin,Emax,n)

Vmin = 0
Vmax = 20
Vrange=np.linspace(Vmin,Vmax,n)

def eigenenergies(E1,E2,V):
    delta = E2-E1
    s = np.sqrt(delta**2+4*V**2)
    Es = [0.5*(E1+E2+s), 0.5*(E1+E2-s)]
    return Es

def probabilities(E1,E2):
    Es = eigenenergies(E1,E2,V)
    Eplus,Eminus = Es
    c1_squared_plus = 1/(1+(Eplus-E1)**2/V**2)
    c1_squared_minus = 1/(1+(Eminus-E1)**2/V**2)
    c2_squared_plus = 1/(1+(Eplus-E2)**2/V**2)
    c2_squared_minus = 1/(1+(Eminus-E2)**2/V**2)
    return [c1_squared_plus,c1_squared_minus,c2_squared_plus,c2_squared_minus]

def c2_from_V(Vf,E1,E2):
    delta = E2-E1
    s = np.sqrt(delta**2+4*Vf**2)
    Eplus,Eminus  = [0.5*(E1+E2+s), 0.5*(E1+E2-s)]
    c2_plus = 1/(1+(Eplus-E2)**2/Vf**2)
    c2_minus = 1/(1+(Eminus-E2)**2/Vf**2)
    return [c2_plus,c2_minus]

def Ediff(E1,E2,V):
    rv = np.zeros(len(E2))
    for i in range(len(E2)):
        Ep,Em = eigenenergies(E1,E2[i],V)
        rv[i] = np.abs(Ep-Em)
    return rv



E1 = np.zeros(n)
E2 = np.zeros(n)
c1p = np.zeros(n)
c1m = np.zeros(n)
c2p = np.zeros(n)
c2m = np.zeros(n)
c2p_V = np.zeros(n)
c2m_V = np.zeros(n)
Ep_V  = np.zeros(n)
Em_V  = np.zeros(n)

for i in range(n):
    ph  = eigenenergies(E_0,E_prime[i],V)
    c1p[i],c1m[i],c2p[i],c2m[i] = probabilities(E_0,E_prime[i])
    c2p_V[i],c2m_V[i] = c2_from_V(Vrange[i],E_0,E_exp)
    E1[i] = ph[0]
    E2[i] = ph[1]
    Ep_V[i],Em_V[i] = eigenenergies(E_0,E_exp,Vrange[i])


plt.figure()
plt.plot(E_prime,E1,label=r"$E_+$")
plt.plot(E_prime,E2,label=r"$E_-$")
plt.xlabel(r"$E'$")
plt.ylabel(r"$E$")
plt.legend()
plt.savefig("fig1.png")
#plt.show()


plt.figure()
plt.plot(E_prime,c1p,label=r"$c_{1+}$")
plt.plot(E_prime,c2p,label=r"$c_{2+}$")
plt.xlabel(r"$E'$")
plt.ylabel(r"$|c|^2$")
plt.ylim([0,1.2])
plt.legend()
plt.savefig("fig2.png")
#plt.show()

plt.figure()
plt.plot(E_prime,c1m,label=r"$c_{1-}$")
plt.plot(E_prime,c2m,label=r"$c_{2-}$")
plt.xlabel(r"$E'$")
plt.ylabel(r"$|c|^2$")
plt.ylim([0,1.2])
plt.legend()
plt.savefig("fig3.png")

plt.figure()
plt.plot(Vrange,c2p_V,label=r"$c_{2+}$")
plt.plot(Vrange,c2m_V,label=r"$c_{2-}$")
plt.xlabel(r"$V$")
plt.ylabel(r"$|c|^2$")
plt.ylim([0,1.2])
plt.legend()
plt.savefig("fig4.png")

plt.figure()
plt.plot(Vrange,Ep_V,label=r"$E_+$")
plt.plot(Vrange,Em_V,label=r"$E_-$")
plt.xlabel(r"$V$")
plt.ylabel(r"$E$")
plt.title(r"$E' = 1.058$ MeV")
plt.legend()
plt.savefig("fig5.png")

V0 = 0.3
V1 = 0.5
V2 = 0.7
V3 = 0.9
V4 = 1.2
Vs = np.linspace(0.51,0.54,7)

plt.figure()
for V in Vs:
    plt.plot(E_prime,Ediff(E_0,E_prime,V),label=r"$V = $"+str(V)+"MeV")
#plt.plot(E_prime,Ediff(E_0,E_prime,V1),label=r"$V = $"+str(V1))
#plt.plot(E_prime,Ediff(E_0,E_prime,V2),label=r"$V = $"+str(V2))
#plt.plot(E_prime,Ediff(E_0,E_prime,V3),label=r"$V = $"+str(V3))
#plt.plot(E_prime,Ediff(E_0,E_prime,V4),label=r"$V = $"+str(V4))
plt.plot(E_prime,np.ones(n)*E_exp,label=r"$1.058$ MeV")
plt.xlabel(r"$E'$")
plt.ylabel(r"$\Delta E$")
plt.xlim([-4,np.max(E_prime)])
plt.legend()
plt.savefig("fig6.png")
plt.show()
