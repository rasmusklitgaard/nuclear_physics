import numpy as np
from scipy import integrate
import matplotlib.pyplot as plt


pi = 3.141592653

R0 = 1.7 #fm
R = 7*R0
dr = 0.01 #fm
U0 = 23 #Our guess on U0
mcc = 938 # MeV / c^2
hc = 200 # MeV*fm
E = 2.2 # MeV
k = np.sqrt(2*mcc*E)/hc # c cancels and MeV cancels so in 1/fm.


def pot_well(r,U0):
    if r>R0:
        U = 0
    else:
        U = -U0
    return U


def pot_yuk(r,f):
    mu =938/200
    mu = 35
    mu = 1/R0
    U = -f**2 * np.exp(-mu*r)/r
    return U


def A(r,pot_type,U0,f):
    if pot_type == 'well':
        U = pot_well(r,U0)

    if pot_type == 'yuk':
        U = pot_yuk(r,f)

    rval = 2*mcc/hc**2*(E+U)
    return rval

def solvr(Y,r,pot_type,U0,f):
    eq1 = Y[1]
    eq2 = A(r,pot_type,U0,f)*Y[0]
    return [eq1,eq2]


r_range = np.arange(0,R,dr)
r_range = np.flip(r_range) #invert

#inital conditions in R
x1_0 = np.exp(-R*k)
x2_0 = -k*x1_0

n = 10000 #runs
for i in range(0,n):
    U0 = U0 + 0.01
    asol = integrate.odeint(solvr,[x1_0,x2_0],r_range,args=('well',U0,0))
    x1 = asol[:,0]
    if((x1[-1]) < 0.0001):
        print("Hellooo")
        print(U0)
        break

print(i,U0)

plt.figure()
plt.plot(r_range,x1,label="l=0 / s-wave")
plt.title("Square well solution")
plt.xlabel("r [fm]")
plt.ylabel("u_l(r)")
plt.legend()
plt.savefig("well_pot.png")

# %%
try_these_f_values = np.linspace(0,500,10000)
for f in try_these_f_values:
    asol = integrate.odeint(solvr,[x1_0,x2_0],r_range,args=('yuk',U0,f))
    x1 = asol[:,0]
    if((x1[-1]) < 0.0001):
        print("Helloo")
        break

plt.figure()
plt.plot(r_range,x1,label="l=0/s-wave")
# plt.plot(r_range,pot_yuk(r_range,f),'k') # plottet potential
plt.xlabel("r [fm]")
plt.ylabel("u_l(r)")
plt.title("Yukawa potential")
plt.legend()
plt.show()
