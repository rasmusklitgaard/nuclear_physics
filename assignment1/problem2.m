clear all; close all; clc;
V0 = 40; % potential in MeV
A = [25,50,75,100,125,150,175,200]; % number of nuclei in number of nuclei
l = 0; % angular momentum 
[~,n] = size(A); % number of A's

% loop over all A
for i=1:n
	A_i = A(1,i) % current A
	E = squareb(V0,A_i,l) % energy solutions
end
