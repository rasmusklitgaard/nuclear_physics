from math import pi
mcc = 938 #MeV
hc  = 200 #MeV fm
R0  = 1.4 #fm
U0  = 40  #MeV
def infWellEnergy(A,n):
    R = R0*A**(1/3)
    L = R
    E = n*n*pi*pi*hc*hc/(2*L*L*mcc)
    return E

A=75
n=1

E_infWell = infWellEnergy(A,n)
E_gap = E_infWell-U0

As = [25,50,75,100,125,150,175,200]
for A in As:
    n1 = infWellEnergy(A,1) -U0
    n2 = infWellEnergy(A,2) -U0
    n3 = infWellEnergy(A,3) -U0
    n4 = infWellEnergy(A,4) -U0
    print("%s %s %s %s" %(n1,n2,n3,n4))


