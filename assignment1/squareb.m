%% Program to calculate energy eigenvalues for neutrons in the potential
%% from a nucleus. The potential is approximated by a square well with a
%% radius 1.4*A^(1/3) fm. 

%% Input to the function is:
%% V0 Depth of potential well (scalar)
%% A Nucleon number for the nucleus (scalar)
%% l Angular momentum quantum number(s) for the neutron (scalar or column vector)

%% Output is:
%% E Energy eigenvalues (vector)
%% l The corresponding angular momenta (vector)

%% The program can be run in MATLAB and in Octave, in the latter case only scalar input for l

%% Changed the program from normal to spherical Bessel functions. 21/6-16

function [E,lvalue] = squareb(V0,A,l)
%close all
%%clc

r0 = 1.4*A^(1/3);
E1 = -1*V0:.1:-.2; %MeV
E1length = length(E1);

index1 = zeropoint(E1,V0,r0,l);
E = zeros(1,length(index1));
lvalue = E;

    for i = 1:length(index1)
       for n = 1:length(l)
           if and(E1length*(n-1)<index1(i),index1(i)<E1length*n)
                lvalue(i) = l(n);
                E2 = E1(index1(i) - E1length*(n-1))-.2:.001:E1(index1(i) - E1length*(n-1))+.2;
                index2 = zeropoint(E2,V0,r0,lvalue(i));
                E(i) = E2(index2);
           end
       end
    end




end


%% Helpfunction
function ichange = zeropoint(E,V0,r0,l)

mhbar = .04826;


k = sqrt(mhbar*(V0-abs(E)));
kappa = sqrt(mhbar*abs(E));

if length(l) == 1
    indre = (k*r0.*besselj(l+1.5,k*r0)./besselj(l+.5,k*r0))';
    ydre = (kappa*r0.*besselk(l+1.5,kappa*r0)./besselk(l+.5,kappa*r0))';
else
    indre = (ones(length(l),1)*k*r0)'.*besselj(l+1.5,k*r0)./besselj(l+.5,k*r0);
    ydre = (ones(length(l),1)*kappa*r0)'.*besselk(l+1.5,kappa*r0)./besselk(l+.5,kappa*r0);
end
%plot(E,indre,'r-')
%hold on
%plot(E,ydre,'b-')
%pause(2)

egenligning = indre - ydre;

signchange = egenligning(1:end-1,:).*egenligning(2:end,:);

ichange = find(signchange<0);

ichange(abs(egenligning(ichange))>10) = [];

end
