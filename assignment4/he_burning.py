import numpy as np
import matplotlib.pyplot as plt
T = 10**2 # in units of 10**6 K so in effect 10**8 K 

z1 = 2 # z of helium 4
z2 = 8 # z of oxygen 16

A1 = 4
A2 = 16
mu = A1*A2/(A1+A2) # in units of 931.494 MeV

E0 = 1.220 * ( z1**2 * z2**2 * mu * T**2)**(1/3) # in keV



Delta = 0.749 * (z1**2*z2**2*mu*T**5)**(1/6) # full width at 1/2/sqrt(2log2) maximum in keV

print(E0)
print(Delta)

print("look around %s +- %s MeV"%((4729+E0)/1000,Delta/1000))

