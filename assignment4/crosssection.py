import matplotlib.pyplot as plt
import numpy as np

n = 10000
E_range = np.linspace(0,6,n)

def cs(a1,a2,E,Er1,Er2,g1,g2):
    t1 = a1/(E-Er1+1/2*1j*g1)
    t2 = a2/(E-Er2+1/2*1j*g2)
    s= t1+t2
    rv = (s.conjugate() * s).real
    return rv

e5 = 2.2
e7 = 3.4
# g5 = 0.6
g5 = 0.1
g7 = 0.6
a5 = 1
a7 = 0.7

points = np.zeros(n)

p1 =  np.zeros(n)
p2 =  np.zeros(n)

for i in range(n):
    points[i] = cs(a5,a7,E_range[i],e5,e7,g5,g7)
    p1[i]     = cs(a5,0,E_range[i],e5,e7,g5,g7)
    p2[i]     = cs(0,a7,E_range[i],e5,e7,g5,g7)

plt.figure()
plt.semilogy(E_range,points,label='Combined system',lw=2)
plt.semilogy(E_range,p1,'--',label='Only peak 5',lw=0.8)
plt.semilogy(E_range,p2,'--',label='Only peak 7',lw=0.8)
plt.xlabel(r"$E_\alpha \ [\mathrm{MeV}]$")
plt.ylabel(r"$\mathrm{Arb}\ \mathrm{units}$")
plt.legend()
plt.savefig("fig_prblm_c.png")
plt.show()
