from math import exp
def v2(x):
    rv = x**4/(9+3*x**2+x**4)
    return rv

# l=2 case

def pen(E,l):# E in MeV
    P = 0
    m1cc = 14900 # mass of 16O in MeV/c^2
    m2cc = 3730 # mass of 4alpha in MeV/c^2
    if(l==2):
        hc = 200 # MeV *fm


        k = (2*E)**0.5 / hc * (m1cc*m2cc/(m1cc+m2cc))**0.5 # k value in 1/fm
        R = 1.4 * 20**(1/3) # roughly radius of neon 20 nucleus, in fm

        P = v2(k*R)*k*R
    elif(l==0):
        z1 = 2
        z2 = 8
        mucc = m1cc*m2cc/(m1cc+m2cc)
        alpha = 1/137
        eta = z1*z2*alpha*(mucc/(2*E))**0.5
        P = exp(-2*3.14159*eta)
    return P



def ffactor(E): # E in MeV

    mecc = 0.511 # MeV/c^2
    return (E/mecc)**5/30



print("pen(1,0) = %s"%(pen(1,0)))
print("pen(3,0) = %s"%(pen(3,0)))
print("pen(5,0) = %s"%(pen(5,0)))
print("pen(1,2) = %s"%(pen(1,2)))
print("pen(3,2) = %s"%(pen(3,2)))
print("pen(5,2) = %s"%(pen(5,2)))



Estart = 13.887 # MeV
Eend   = 11.331 # MeV
Eavail = Estart-Eend

me = 0.510 # MeV

Eavail -= 2*me # now all energy is kinetic energy available for the positron
Eavail += me
print("f value, with %s MeV available for positron kinetic energy: %s"%(Eavail,ffactor(Eavail)))




for E in [1,3,5]:
    print("penetration probabilty for %s MeV = %s"%(E,pen(E,0)*pen(E,2)))

for E_theoretical_end in [1,3,5]:
    E = Estart - E_theoretical_end - me
    print("f value for ending point in %s Mev : f = %s"%(E_theoretical_end,ffactor(E)))









